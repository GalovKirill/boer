using Boer;
using Xunit;

namespace BoerTests
{
    public class TagSetTest
    {
        [Fact]
        public void CountTest()
        {
            TagSet set1 = new TagSet(new Tag[] {1, 2, 3});
            int expected = 3;
            int actual = set1.Count;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ClearTest()
        {
            TagSet set1 = new TagSet(new Tag[] {1, 2, 3});
            set1.Clear();
            int expected = 0;
            int actual = set1.Count;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Contains()
        {
            TagSet set1 = new TagSet(new Tag[] {1, 2, 3});
            bool actual1 = set1.Contains(1);
            Assert.True(actual1);
            
            bool actual2 = set1.Contains(5);
            Assert.False(actual2);
        }
    }
}