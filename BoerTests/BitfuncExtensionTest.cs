using System;
using System.Diagnostics;
using Boer;
using Xunit;

namespace BoerTests
{
    public class BitfuncExtensionTest
    {
        [Fact]
        public void BitIsTrueTest()
        {
            const ulong r = 0b010UL;
            Assert.False(r.BitIsTrue(0));
            Assert.True(r.BitIsTrue(1));
            Assert.False(r.BitIsTrue(2));
        }

        [Fact]
        public void CountTrueBit()
        {
            const ulong r = 0b0101010UL; // 3 true bit        
            Assert.Equal(3, r.CountTrueBit());
        }

        [Fact]
        public void SetBitValueTest()
        {
            var r = 0UL;
            r.SetBitValue(0,true);
            Assert.Equal(1UL, r);
            Assert.True(r.BitIsTrue(0));
            r.SetBitValue(0, false);
            Assert.False(r.BitIsTrue(0));
        }
    }
}