namespace Boer
{
    public struct Tag
    {
        public Tag(int id)
        {
            Id = id;
        }

        public int Id { get; }

        public static implicit operator Tag(int id)
        {
            return new Tag(id);
        }
        
        public static implicit operator int(Tag tag)
        {
            return tag.Id;
        }
    }
}