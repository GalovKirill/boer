using System;
using System.Collections;
using System.Collections.Generic;
namespace Boer
{
    public unsafe struct TagSet : ISet<Tag>
    {
        private const int CountR = 4;

        private fixed ulong _rs[CountR];

        public TagSet(IEnumerable<Tag> tags)
        {
            foreach (var tag in tags)
            {
                Add(tag);
            }
        }

        public IEnumerator<Tag> GetEnumerator()
        {
            for (int i = 0; i < CountR; i++)
            {
                ulong r = GetRegisterByTagId(i);
                for (int j = 0; j < sizeof(ulong); j++)               
                    if (r.BitIsTrue(j))               
                        yield return new Tag(i * sizeof(ulong) + j);         
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        void ICollection<Tag>.Add(Tag item)
        {
            Add(item);
        }

        void ISet<Tag>.ExceptWith(IEnumerable<Tag> other)
        {
            var set = new TagSet(other);
            ExceptWith(set);
        }
        
        public void ExceptWith(in TagSet other)
        {
            for (int i = 0; i < CountR; i++)
                _rs[i] &= ~other[i];
        }

        void ISet<Tag>.IntersectWith(IEnumerable<Tag> other)
        {
            var set = new TagSet(other);
            IntersectWith(set);
        }

        public void IntersectWith(in TagSet other)
        {
            for (int i = 0; i < CountR; i++)
                _rs[i] &= other[i];
        }

        public bool IsProperSubsetOf(IEnumerable<Tag> other)
        {
            throw new System.NotImplementedException();
        }

        public bool IsProperSupersetOf(IEnumerable<Tag> other)
        {
            throw new System.NotImplementedException();
        }

        public bool IsSubsetOf(IEnumerable<Tag> other)
        {
            throw new System.NotImplementedException();
        }

        public bool IsSupersetOf(IEnumerable<Tag> other)
        {
            throw new System.NotImplementedException();
        }

        public bool Overlaps(IEnumerable<Tag> other)
        {
            throw new System.NotImplementedException();
        }

        public bool SetEquals(IEnumerable<Tag> other)
        {
            throw new System.NotImplementedException();
        }
        

        public void SymmetricExceptWith(IEnumerable<Tag> other)
        {
            throw new System.NotImplementedException();
        }

        void ISet<Tag>.UnionWith(IEnumerable<Tag> other)
        {
            var set = new TagSet(other);
            UnionWith(set);
        }
        
        public void UnionWith(in TagSet other)
        {
            for (int i = 0; i < CountR; i++)
                _rs[i] ^= other[i];
        }

        public bool Add(Tag item)
        {
            ref ulong r = ref GetRegisterByTagId(item.Id);
            var index = item.Id.ToIndex64();
            if (r.BitIsTrue(index))
                return false;
            r.SetBitValue(index, true);
            return true;
        }

        public void Clear()
        {
            for (int i = 0; i < CountR; i++)
                _rs[i] = 0UL;
        }

        public bool Contains(Tag item)
        {
            return GetRegisterByTagId(item).BitIsTrue(item.Id.ToIndex64());
        }

        public void CopyTo(Tag[] array, int arrayIndex)
        {
            if (arrayIndex + Count > array.Length)
            {
                throw new IndexOutOfRangeException();
            }
            
            foreach (var tag in this)
            {
                array[arrayIndex] = tag;
                arrayIndex++;
            }
        }

        public bool Remove(Tag item)
        {
            ulong r = GetRegisterByTagId(item);
            var index = item.Id.ToIndex64();
            if (!r.BitIsTrue(index)) return false;
            r.SetBitValue(index, false);
            return true;
        }

        public int Count
        {
            get
            {
                int count = 0;
                for (int i = 0; i < CountR; i++)
                    count += _rs[i].CountTrueBit();
                return count;
            }
        }
        
        public bool IsReadOnly => false;

        private ref ulong GetRegisterByTagId(int index)
        {
            return ref _rs[index / (8 * sizeof(ulong))];
        }

        private ulong this[int i] => _rs[i];
    }
}