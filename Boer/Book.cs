namespace Boer
{
    public class Book
    {
        public string FileName { get; set; }
        public string Author { get; set; }
        public TagSet TagSet { get; set; }
    }
}