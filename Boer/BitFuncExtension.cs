using System;

namespace Boer
{
    internal static class BitFuncExtension
    {
        private const int UlongSize = 8 * sizeof(ulong);
        public static bool BitIsTrue(this ulong r, int index)
        {   
            return index < UlongSize && (r & (1UL << index)) != 0;
        }

        public static int ToIndex64(this int i)
        {
            return i % UlongSize;
        }

        public static int CountTrueBit(this ulong r)
        {
            int count = 0;
            for (int i = 0; i < UlongSize; i++)
            {
                if (r.BitIsTrue(i))
                    count++;
            }
            return count;
        }

        public static void SetBitValue(this ref ulong r, int index, bool value)
        {
            ulong valueBit = 1UL << index;
            if (!value)
            {
                valueBit = ~valueBit;
                r &= valueBit;
            }
            else
            {
                r |= valueBit;
            }
            
        }
        
        
    }
}